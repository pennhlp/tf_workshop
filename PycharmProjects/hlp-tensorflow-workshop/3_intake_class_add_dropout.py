'''Convolutional Neural Network for a classification task'''
from __future__ import print_function

import argparse
import re

import numpy as np
import tensorflow as tf
from tensorflow.contrib import learn


class CNNModel(object):
    """
    A CNN for text classification.
    Uses an embedding layer, followed by a convolutional, max-pooling and softmax layer.
    """
    def __init__(self, sequence_length, num_classes, vocab_size,
                 embedding_size, filter_sizes, num_filters, lrn_rate=0.001):

        # Placeholders for input, output and dropout
        self.input_x = tf.placeholder(tf.int32, [None, sequence_length], name="input_x")
        self.input_y = tf.placeholder(tf.float32, [None, num_classes], name="input_y")
        self.dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")

        # Embedding layer
        with tf.device('/cpu:0'), tf.name_scope("embedding"):
            self.wordemb = tf.Variable(tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0))
            self.embedded_chars = tf.nn.embedding_lookup(self.wordemb, self.input_x)
            self.embedded_chars_expanded = tf.expand_dims(self.embedded_chars, -1)

        # Create a convolution + maxpool layer for each filter size
        pooled_outputs = []
        for _, filter_size in enumerate(filter_sizes):
            with tf.name_scope("conv-maxpool-%s" % filter_size):
                # Convolution Layer
                filter_shape = [filter_size, embedding_size, 1, num_filters]
                weights = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.1), name="W")
                bias = tf.Variable(tf.constant(0.1, shape=[num_filters]), name="b")
                conv = tf.nn.conv2d(self.embedded_chars_expanded, weights, strides=[1, 1, 1, 1],
                                    padding="VALID", name="conv")
                # Apply nonlinearity
                relu = tf.nn.relu(tf.nn.bias_add(conv, bias), name="relu")
                # Maxpooling over the outputs
                pooled = tf.nn.max_pool(relu, ksize=[1, sequence_length - filter_size + 1, 1, 1],
                                        strides=[1, 1, 1, 1], padding='VALID', name="pool")
                pooled_outputs.append(pooled)

        # Combine all the pooled features
        num_filters_total = num_filters * len(filter_sizes)
        self.h_pool = tf.concat(pooled_outputs, 3)
        self.h_pool_flat = tf.reshape(self.h_pool, [-1, num_filters_total])

        # Add dropout
        with tf.name_scope("dropout"):
            self.h_drop = tf.nn.dropout(self.h_pool_flat, self.dropout_keep_prob)

        # Final (unnormalized) scores and predictions
        with tf.name_scope("output"):
            weight = tf.get_variable("W", shape=[num_filters_total, num_classes],
                                     initializer=tf.contrib.layers.xavier_initializer())
            bias = tf.Variable(tf.constant(0.1, shape=[num_classes]), name="b")
            self.scores = tf.nn.xw_plus_b(self.h_drop, weight, bias, name="scores")

            self.predictions = tf.argmax(self.scores, 1, name="predictions")

        # CalculateMean cross-entropy loss
        with tf.name_scope("loss"):
            losses = tf.nn.softmax_cross_entropy_with_logits(logits=self.scores,
                                                             labels=self.input_y)
            self.loss = tf.reduce_mean(losses) 
            self.cost = self.loss

        # CalculateMean cross-entropy loss
        with tf.name_scope("optimize"):
            self.optimizer = tf.train.AdamOptimizer(learning_rate=lrn_rate).minimize(self.loss)

        # Accuracy
        with tf.name_scope("accuracy"):
            correct_predictions = tf.equal(self.predictions, tf.argmax(self.input_y, 1))
            self.accuracy = tf.reduce_mean(tf.cast(correct_predictions, "float"), name="accuracy")


def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    # string = re.sub(r"^https?:\/\/.*", " URL ", string)
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", r" \( ", string)
    string = re.sub(r"\)", r" \) ", string)
    string = re.sub(r"\?", r" \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip()
import string
def load_intake_data(input_file):
    """Loads data and labels"""
    texts = []
    labels = []
    for line in open(input_file):
        parts = line.split('\t')
        if len(parts) == 6:
            text = clean_str(parts[4])
            texts.append(text)
            label = string.strip(parts[-1])
            if label == '1':
                labels.append(np.array([1, 0, 0]))
            elif label == '2':
                labels.append(np.array([0, 1, 0]))
            elif label == '3':
                labels.append(np.array([0, 0, 1]))
            else:
                print("Invalid label {} found for text {}".format(label, text))
    print(input_file, len(texts), len(labels))
    assert len(texts) == len(labels)
    return texts, labels

def get_word_emb(emb_loc, vocab_processor):
    """Loads word embeddings"""
    if emb_loc.split('.')[-1] == 'bin':
        # load the bin file format
        with open(emb_loc, "rb") as emb_file:
            header = emb_file.readline()
            vocab_size, emb_dim = map(int, header.split())
            print("Loading word2vec file {}".format(emb_loc))
            print("Emb prop: vocab={} and dim={}\n".format(vocab_size, emb_dim))
            initw = np.random.uniform(-0.25, 0.25, (len(vocab_processor.vocabulary_), emb_dim))
            binary_len = np.dtype('float32').itemsize * emb_dim
            for _ in xrange(vocab_size):
                word = []
                while True:
                    char = emb_file.read(1)
                    if char == ' ':
                        word = ''.join(word)
                        break
                    if char != '\n':
                        word.append(char)
                wordid = vocab_processor.vocabulary_.get(word)
                if wordid != 0:
                    initw[wordid] = np.fromstring(emb_file.read(binary_len), dtype='float32')
                else:
                    emb_file.read(binary_len)
        vocab_size = len(vocab_processor.vocabulary_)
    else:
        # glove format files
        pass
    print("WordEmb shape", initw.shape)
    return initw, emb_dim, vocab_size

def f1score(pred, targ, num_classes, ignore_last_index=False):
    """Computes f1 score"""
    pred = np.argmax(pred, axis=1)
    targ = np.argmax(targ, axis=1)
    true_pos = np.array([0] * (num_classes + 1))
    false_pos = np.array([0] * (num_classes + 1))
    false_neg = np.array([0] * (num_classes + 1))
    for i, label in enumerate(targ):
        if label == pred[i]:
            true_pos[label] += 1
        else:
            false_pos[label] += 1
            false_neg[pred[i]] += 1
    valid_classes = num_classes-1 if ignore_last_index else num_classes
    for i in range(valid_classes):
        true_pos[num_classes] += true_pos[i]
        false_pos[num_classes] += false_pos[i]
        false_neg[num_classes] += false_neg[i]
    precision = []
    recall = []
    fscore = []
    for i in range(num_classes + 1):
        precision.append(true_pos[i] * 1.0 / (true_pos[i] + false_pos[i]))
        recall.append(true_pos[i] * 1.0 / (true_pos[i] + false_neg[i]))
        fscore.append(2.0 * precision[i] * recall[i] / (precision[i] + recall[i]))
    return (precision[0] + precision[1]) / 2, (recall[0] + recall[1]) / 2, (fscore[0] + fscore[1]) / 2

    #return precision[num_classes], recall[num_classes], fscore[num_classes]

def train(args):
    '''Training method'''
    # Load training, test and validation tokens, vector instances (input vector) and labels
    train_t, train_l = load_intake_data(args.train)
    dev_t, dev_l = load_intake_data(args.dev)
    test_t, test_l = load_intake_data(args.test)
    max_seq_len = max([len(x.split()) for x in train_t+dev_t+test_t])
    vocab_processor = learn.preprocessing.VocabularyProcessor(max_seq_len)
    train_test_combined = np.array(list(vocab_processor.fit_transform(train_t+dev_t+test_t)))
    train_t = train_test_combined[:len(train_t)]
    dev_t = train_test_combined[len(train_t):len(train_t)+len(dev_t)]
    test_t = train_test_combined[len(train_t)+len(dev_t):]
    word_emb, emb_dim, vocab_size = get_word_emb(args.emb_loc, vocab_processor)
    filter_sizes = [int(x) for x in args.filter_sizes.split(',')]
    print("Max len:", max_seq_len, "Input size:", emb_dim, "Vocab size:", vocab_size,
          "Filter sizes:", filter_sizes)
    # Create model
    model = CNNModel(max_seq_len, args.num_classes, vocab_size, emb_dim,
                     filter_sizes, args.num_filters, args.lrn_rate)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(model.wordemb.assign(word_emb))
        saver = tf.train.Saver()
        def evaluate(instances, labels, write_result=False):
            '''Evaluate and print results'''
            prediction = sess.run(model.scores, feed_dict={
                model.input_x: np.asarray(instances),
                model.dropout_keep_prob: 1.0})
            precision, recall, f1sc = f1score(prediction, np.asarray(labels), args.num_classes)
            if write_result:
                # MAX FOUND. Write results to file
                print("Max Found\tP:{:.5f}, R:{:.5f}, F1:{:.5f}".format(precision, recall, f1sc))
            return f1sc
        # Training cycle
        if args.train_epochs > 0:
            maxf1 = 0.0
            for epoch in range(args.train_epochs):
                avg_cost = 0.0
                total_batch = int(len(train_t)/args.batch_size)
                for ptr in range(0, len(train_t), args.batch_size):
                    _, epoch_cost = sess.run([model.optimizer, model.cost], feed_dict={
                        model.input_x: np.asarray(train_t[ptr:ptr + args.batch_size]),
                        model.input_y: np.asarray(train_l[ptr:ptr + args.batch_size]),
                        model.dropout_keep_prob: 0.5})
                    # Compute average loss across batches
                    avg_cost += epoch_cost / total_batch
                if epoch % args.eval_interval == 0:
                    train_f1 = evaluate(train_t, train_l)
                    val_f1 = evaluate(dev_t, dev_l)
                    print("Epoch:", '%02d' % (epoch+1), "cost=", "{:.5f}".format(avg_cost),
                          "-Training : {:.5f}".format(train_f1), "Val : {:.5f}".format(val_f1))
                    if val_f1 > maxf1:
                        maxf1 = val_f1
                        if args.save is not None:
                            # Write model checkpoint to disk
                            print("Saving model to {}".format(args.save))
                            saver.save(sess, args.save)
            print("Optimization Finished!")
        # Load best model and evaluate model on the test set before applying to production
        print("Restoring model from {}".format(args.restore))
        if args.restore is not None:
            saver.restore(sess, args.restore)
            print("Model from {} restored.".format(args.restore))
        evaluate(test_t, test_l, True)

def main():
    '''Main method : parse input arguments and train'''
    parser = argparse.ArgumentParser()
    # Input files
    parser.add_argument('--train', type=str, default='data/task2/intake_train.txt',
                        help='train file location')
    parser.add_argument('--dev', type=str, default='data/task2/intake_dev.txt',
                        help='dev set file location')
    parser.add_argument('--test', type=str, default='data/task2/intake_test.txt',
                        help='test file location')
    # Word Embeddings
    parser.add_argument('--emb_loc', type=str, default="data/5M.bin",
                        help='word2vec embedding location')
    # Hyperparameters
    parser.add_argument('--lrn_rate', type=float, default=0.001, help='learning rate')
    parser.add_argument('--filter_sizes', type=str, default='3,5',
                        help='sizes of filters, comma delimited')
    parser.add_argument('--num_filters', type=int, default=50,
                        help='Number of filters, i.e. dimensions for weights in each filters')
    # Settings
    parser.add_argument('--train_epochs', type=int, default=15, help='number of training epochs')
    parser.add_argument('--eval_interval', type=int, default=1, help='evaluate once in _ epochs')
    parser.add_argument('--num_classes', type=int, default=3, help='number of classes')
    parser.add_argument('--batch_size', type=int, default=128, help='batch size of training')
    # Model save and restore paths
    parser.add_argument('--restore', type=str, default="model/cnntask2", help="path of saved model")
    parser.add_argument('--save', type=str, default="model/cnntask2", help="path to save model")
    args = parser.parse_args()
    print(clean_str("This couldn't be a string with \"quotes\" and URLs but... https://abcd.com/adr-123/65 is a string after-all."))
    train(args)

if __name__ == '__main__':
    main()
